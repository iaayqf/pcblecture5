EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:simpleswitcher
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LMR14050 U1
U 1 1 5AB296B3
P 5600 3650
F 0 "U1" H 5250 4150 60  0000 L CNN
F 1 "LMR14050" H 5250 4050 60  0000 L CNN
F 2 "" H 5050 3900 60  0001 C CNN
F 3 "" H 5050 3900 60  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
$Comp
L L L1
U 1 1 5AB29756
P 7750 3600
F 0 "L1" V 7700 3600 50  0000 C CNN
F 1 "2.20uH" V 7825 3600 50  0000 C CNN
F 2 "" H 7750 3600 50  0001 C CNN
F 3 "" H 7750 3600 50  0001 C CNN
	1    7750 3600
	0    1    1    0   
$EndComp
$Comp
L D_Schottky D1
U 1 1 5AB297EF
P 7400 3750
F 0 "D1" H 7400 3850 50  0000 C CNN
F 1 "0.455V, 20A" H 7550 3650 50  0000 C CNN
F 2 "" H 7400 3750 50  0001 C CNN
F 3 "" H 7400 3750 50  0001 C CNN
	1    7400 3750
	0    1    1    0   
$EndComp
$Comp
L C C4
U 1 1 5AB29875
P 8150 3750
F 0 "C4" H 8175 3850 50  0000 L CNN
F 1 "180uF" H 8175 3650 50  0000 L CNN
F 2 "" H 8188 3600 50  0001 C CNN
F 3 "" H 8150 3750 50  0001 C CNN
	1    8150 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3600 7600 3600
Connection ~ 7400 3600
Wire Wire Line
	7900 3600 10250 3600
Wire Wire Line
	7400 3900 8150 3900
$Comp
L GND #PWR01
U 1 1 5AB29938
P 7750 3900
F 0 "#PWR01" H 7750 3650 50  0001 C CNN
F 1 "GND" H 7750 3750 50  0000 C CNN
F 2 "" H 7750 3900 50  0001 C CNN
F 3 "" H 7750 3900 50  0001 C CNN
	1    7750 3900
	1    0    0    -1  
$EndComp
Connection ~ 7750 3900
Connection ~ 8150 3600
$Comp
L R R2
U 1 1 5AB29963
P 8950 3950
F 0 "R2" V 8850 3950 50  0000 C CNN
F 1 "68.0K, 1%" V 9050 3950 50  0000 C CNN
F 2 "" V 8880 3950 50  0001 C CNN
F 3 "" H 8950 3950 50  0001 C CNN
	1    8950 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 3600 8950 3800
Connection ~ 8950 3600
$Comp
L R R3
U 1 1 5AB299E1
P 8950 4500
F 0 "R3" V 8850 4500 50  0000 C CNN
F 1 "12.0K, 1%" V 9050 4500 50  0000 C CNN
F 2 "" V 8880 4500 50  0001 C CNN
F 3 "" H 8950 4500 50  0001 C CNN
	1    8950 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AB29A17
P 8950 4650
F 0 "#PWR02" H 8950 4400 50  0001 C CNN
F 1 "GND" H 8950 4500 50  0000 C CNN
F 2 "" H 8950 4650 50  0001 C CNN
F 3 "" H 8950 4650 50  0001 C CNN
	1    8950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 4100 8950 4350
Wire Wire Line
	6850 4250 8950 4250
Wire Wire Line
	6850 4250 6850 3800
Wire Wire Line
	6850 3800 6250 3800
Connection ~ 8950 4250
Wire Wire Line
	5050 3800 4850 3800
Wire Wire Line
	4850 3800 4850 4150
$Comp
L GND #PWR03
U 1 1 5AB29A6E
P 4050 4300
F 0 "#PWR03" H 4050 4050 50  0001 C CNN
F 1 "GND" H 4050 4150 50  0000 C CNN
F 2 "" H 4050 4300 50  0001 C CNN
F 3 "" H 4050 4300 50  0001 C CNN
	1    4050 4300
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5AB29A8B
P 4300 3900
F 0 "C2" H 4325 4000 50  0000 L CNN
F 1 "8.20nF" H 4325 3800 50  0000 L CNN
F 2 "" H 4338 3750 50  0001 C CNN
F 3 "" H 4300 3900 50  0001 C CNN
	1    4300 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3700 4300 3700
Wire Wire Line
	4300 3700 4300 3750
Wire Wire Line
	4300 4150 4300 4050
$Comp
L R R1
U 1 1 5AB29B65
P 3850 3950
F 0 "R1" V 3930 3950 50  0000 C CNN
F 1 "20.5K, 1%" V 3750 3950 50  0000 C CNN
F 2 "" V 3780 3950 50  0001 C CNN
F 3 "" H 3850 3950 50  0001 C CNN
	1    3850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3600 3850 3600
Wire Wire Line
	3850 3600 3850 3800
Wire Wire Line
	3850 4100 3850 4150
$Comp
L C C1
U 1 1 5AB29C6A
P 3250 3850
F 0 "C1" H 3275 3950 50  0000 L CNN
F 1 "10uF" H 3275 3750 50  0000 L CNN
F 2 "" H 3288 3700 50  0001 C CNN
F 3 "" H 3250 3850 50  0001 C CNN
	1    3250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3400 5050 3400
Wire Wire Line
	3250 3400 3250 3700
Wire Wire Line
	3250 4000 3250 4150
Wire Wire Line
	3250 4150 4850 4150
Connection ~ 3850 4150
Connection ~ 4300 4150
Wire Wire Line
	4050 4150 4050 4300
Connection ~ 4050 4150
Wire Wire Line
	5050 3500 4750 3500
Wire Wire Line
	4750 3500 4750 3400
Connection ~ 4750 3400
$Comp
L C C5
U 1 1 5AB2A080
P 8550 3950
F 0 "C5" H 8575 4050 50  0000 L CNN
F 1 "200pF" H 8575 3850 50  0000 L CNN
F 2 "" H 8588 3800 50  0001 C CNN
F 3 "" H 8550 3950 50  0001 C CNN
	1    8550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3800 8550 3600
Connection ~ 8550 3600
Wire Wire Line
	8550 4100 8550 4250
Connection ~ 8550 4250
$Comp
L C C3
U 1 1 5AB2A2C1
P 6900 3450
F 0 "C3" H 6925 3550 50  0000 L CNN
F 1 "100nF" H 6925 3350 50  0000 L CNN
F 2 "" H 6938 3300 50  0001 C CNN
F 3 "" H 6900 3450 50  0001 C CNN
	1    6900 3450
	1    0    0    -1  
$EndComp
Connection ~ 6900 3600
Wire Wire Line
	6900 3300 6500 3300
Wire Wire Line
	6500 3300 6500 3400
Wire Wire Line
	6500 3400 6250 3400
$Comp
L Conn_01x02 J1
U 1 1 5AB2A3B4
P 1950 3500
F 0 "J1" H 1950 3600 50  0000 C CNN
F 1 "Vin" H 1950 3300 50  0000 C CNN
F 2 "" H 1950 3500 50  0001 C CNN
F 3 "" H 1950 3500 50  0001 C CNN
	1    1950 3500
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02 J2
U 1 1 5AB2A438
P 10450 3600
F 0 "J2" H 10450 3700 50  0000 C CNN
F 1 "Vout" H 10450 3400 50  0000 C CNN
F 2 "" H 10450 3600 50  0001 C CNN
F 3 "" H 10450 3600 50  0001 C CNN
	1    10450 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AB2A55E
P 9950 3700
F 0 "#PWR04" H 9950 3450 50  0001 C CNN
F 1 "GND" H 9950 3550 50  0000 C CNN
F 2 "" H 9950 3700 50  0001 C CNN
F 3 "" H 9950 3700 50  0001 C CNN
	1    9950 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3700 10250 3700
Connection ~ 3250 3400
$Comp
L GND #PWR05
U 1 1 5AB2A600
P 2350 4050
F 0 "#PWR05" H 2350 3800 50  0001 C CNN
F 1 "GND" H 2350 3900 50  0000 C CNN
F 2 "" H 2350 4050 50  0001 C CNN
F 3 "" H 2350 4050 50  0001 C CNN
	1    2350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3500 2350 3500
Wire Wire Line
	2350 3500 2350 4050
$Comp
L PWR_FLAG #FLG06
U 1 1 5AB2A66D
P 2550 3400
F 0 "#FLG06" H 2550 3475 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 3550 50  0000 C CNN
F 2 "" H 2550 3400 50  0001 C CNN
F 3 "" H 2550 3400 50  0001 C CNN
	1    2550 3400
	1    0    0    -1  
$EndComp
Connection ~ 2550 3400
$Comp
L PWR_FLAG #FLG07
U 1 1 5AB2A6A8
P 9550 3600
F 0 "#FLG07" H 9550 3675 50  0001 C CNN
F 1 "PWR_FLAG" H 9550 3750 50  0000 C CNN
F 2 "" H 9550 3600 50  0001 C CNN
F 3 "" H 9550 3600 50  0001 C CNN
	1    9550 3600
	1    0    0    -1  
$EndComp
Connection ~ 9550 3600
$Comp
L PWR_FLAG #FLG08
U 1 1 5AB2A6DC
P 2050 3900
F 0 "#FLG08" H 2050 3975 50  0001 C CNN
F 1 "PWR_FLAG" H 2050 4050 50  0000 C CNN
F 2 "" H 2050 3900 50  0001 C CNN
F 3 "" H 2050 3900 50  0001 C CNN
	1    2050 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3900 2350 3900
Connection ~ 2350 3900
$EndSCHEMATC
